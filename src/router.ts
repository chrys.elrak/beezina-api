import {Request, Response, Router} from "express";
import {router as AuthRoute} from 'auth';
import {initGoogleOAuth, JwtAuth} from "common/services/PassportService";
import {router as ProjectOwnerRoute} from "project-owner";
import {router as SponsorRoute} from "sponsor";
import {router as AnonymeRoute} from "anonyme";
import passport from "passport";
import {IResponse} from "./modules/common/interfaces/IResponse";
import {CategoryModel} from "./modules/common/models/CategoryModel";
import {validate} from "./modules/common/middlewares/formValidation";
import {CategorySchema} from "./modules/common/schemas/CategorySchema";
import {UserClass} from "./modules/common/classes/UserClass";
import {UserModel} from "./modules/common/models/UserModel";
import {eMethods} from "./modules/common/enums/eMethods";

export const router: Router = Router();

// passport.initialize();
// passport.serializeUser(function (user, cb) {
//     cb(null, user);
// });
// passport.deserializeUser(function (obj: any, cb) {
//     cb(null, obj);
// });
// initGoogleOAuth();

JwtAuth().catch(err => {
    console.error(err);
});

// router.use(passport.initialize());

router.use([AuthRoute, AnonymeRoute]);
router.use('/project-owner', ProjectOwnerRoute);
router.use('/sponsor', SponsorRoute);
//
// router.route('/auth/google/callback')
//     .get(
//         passport.authenticate('google-token', {failureRedirect: '/error'}),
//         async (req: Request, res: Response) => {
//             const profile = req.user as any;
//             const user: UserClass = await UserModel.find({
//                 $and: [
//                     {method: eMethods.google},
//                     {'google.id': profile._json.sub}
//                 ]
//             });
//
//             if (user) {
//                 // LOGIN
//             }
//             // REGISTER
//             const newUser = {
//                 google: {...profile._json},
//                 email: {
//                     code: '',
//                     isVerified: profile._json.email_verified,
//                     address: profile._json.email
//                 },
//                 method: profile.provider,
//             };
//             // await (newUser as UserClass).save();
//             res.header('Content-type', 'text/html');
//             return res.send(`You can close this window!`);
//         }
//     );

router.route('/category')
    .get(
        passport.authenticate('jwt', {session: false}),
        async (req: Request, res: Response) => {
            const CategorySchema = await CategoryModel.find();
            return res.status(200).json({
                success: true,
                message: "list of category",
                data: CategorySchema,
                status: 200
            });
        }
    )
    .post(
        // TODO: SECURE FOR ADMIN USER
        validate(CategorySchema),
        async (req: Request, res: Response) => {
            try {
                const categoryAlreadyExist = await CategoryModel.findOne({name: req.body.value.name});
                if (categoryAlreadyExist) {
                    return res.status(409).json({
                        status: 409,
                        success: false,
                        message: "category already exist"
                    } as IResponse);
                }
                const category = new CategoryModel(req.body.value);
                await category.save();
                return res.status(201).json({
                    success: true,
                    message: "category created successfully",
                    status: 201
                } as IResponse);
            } catch (e) {
                throw e;
            }

        }
    )


router.route('/get-user').get(
    passport.authenticate('jwt', {session: false}),
    (req: Request, res: Response) => {
        return res.status(200).json(
            {
                data: req.user,
                message: 'user data',
                status: 200,
                success: true
            } as IResponse
        )
    }
)




