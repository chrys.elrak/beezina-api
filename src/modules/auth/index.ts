import {NextFunction, Request, Router} from 'express';
import {validate} from 'common/middlewares/formValidation';
import {login} from './controllers/LoginController';
import {register, resendCode, confirmation} from './controllers/RegisterController';
import UserSchema, {code_validation} from "common/schemas/UserSchema";
import {resetPassword} from './controllers/ResetController';
import passport from "passport";

export const router: Router = Router();

router.route('/register')
    .post(validate(UserSchema.register), register);
router.route('/register/resend-code-validation')
    .post(resendCode);
router.route('/register/confirmation-code/:id')
    .post(validate(code_validation), confirmation);
router.route('/login')
    .post(validate(UserSchema.login), login);
router.route('/login/reset-password')
    .post(validate(UserSchema.reset_password), resetPassword);
// router.route('/OAuth/google')
//     .get(passport.authenticate('google', {scope: ['profile', 'email']}));
