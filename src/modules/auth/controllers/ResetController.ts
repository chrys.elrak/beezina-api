import {Request, Response} from "express";
import {UserClass} from "common/classes/UserClass";
import {UserModel} from "common/models/UserModel";
import {IResponse} from "common/interfaces/IResponse";
import {cryptoPassword} from "common/services/passwordService";

export const resetPassword = async (req: Request, res: Response) => {
    const value = req.body.value;

    const user: UserClass = await UserModel.findById(value.id);

    if (!user) {
        return res.status(404).json({
            message: 'Account not found, please create an account',
            success: false,
            status: 404
        } as IResponse);
    }

    user.local.password = cryptoPassword(value.password);
    await user.save();

    return res.status(200).json({
        message: "Password was changed successfully",
        success: true,
        status: 200
    } as IResponse);
};