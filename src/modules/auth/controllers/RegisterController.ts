import {Request, Response, NextFunction} from 'express';
import {registerObject} from 'common/schemas/UserSchema'
import {UserModel} from "../../common/models/UserModel";
import {IResponse} from "../../common/interfaces/IResponse";
import {sendCodeValidation} from '../../common/services/validationService';
import {generateCode} from "common/helpers/string";
import {cryptoPassword} from 'common/services/passwordService';
import {IUser} from 'common/interfaces/IUser';
import {UserClass} from "../../common/classes/UserClass";

export async function register(req: Request, res: Response, next: NextFunction) {
    try {
        const value: typeof registerObject = req.body.value;
        const username = await UserModel.findOne({
            'local.username': value.username
        });
        if (username) {
            // Username already taken
            return res.status(400).json({
                message: 'Username already taken',
                status: 400,
                success: false,
            } as IResponse);
        }
        const email = await UserModel.findOne({
            'email.address': value.email
        });
        if (email) {
            // Email already taken
            return res.status(400).json({
                message: 'Email already taken',
                status: 400,
                success: false,
            } as IResponse);
        }

        const user = new UserModel({
            local: {
                username: value.username,
                password: cryptoPassword(value.password.toString())
            },
            'email.address': value.email,
            'phone.number': value.phone,
            role: value.role,
            gender: value.gender
        });

        // Generate code validation if only object is not verified 
        if (!user.email.isVerified) user.email.code = generateCode();
        if (user.phone.number && !user.phone.isVerified) user.phone.code = generateCode();

        await user.save();
        /**
         * TODO How to block the result and pass to clause 'catch' if this promise function provide an error?
         */
        await sendCodeValidation(user).catch(err => {
            throw err;
        });
        const data = {
            "user_id": user._id
        };
        return res.status(200).json({
            status: 200,
            success: true,
            message: 'User registered successfully, verify your email',
            data: data
        } as IResponse);
    } catch (e) {
        next(e);
    }
}

export async function resendCode(req: Request, res: Response, next: NextFunction) {
    const id = req.body.user_id;
    try {
        const user: IUser = await UserModel.findById(id);
        if (!user) {
            return res.status(404).json({
                status: 404,
                success: false,
                message: `${id} not found`
            } as IResponse);
        }
        await sendCodeValidation(user);
        return res.status(200).json({
            status: 200,
            success: true,
            message: 'Code confirmation sent successfully, check your email or/and verify your phone message'
        } as IResponse);
    } catch (e) {
        next(e);
    }
}

export const confirmation = async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id || null;
    if (!id) {
        return res.status(400).json({
            message: 'id on request params is required',
            success: false,
            status: 400
        } as IResponse);
    }
    try {
        const {code} = req.body.value;
        const user: UserClass = await UserModel.findById(id);

        if (!user) {
            return res.status(404).json({
                message: 'Account not found, please create an account',
                success: false,
                status: 404
            } as IResponse);
        }

        if (user.email.isVerified) {
            return res.status(400).json({
                message: 'Your account is already verified',
                success: true,
                status: 400
            } as IResponse);
        }

        if (user.email.code === code) {
            user.email.isVerified = true;
            user.email.code = '';
            await user.save();
            return res.status(200).json({
                message: 'Verification account successfully',
                success: true,
                status: 200
            } as IResponse);
        }
        return res.status(403).json({
            message: 'Code confirmation incorrect',
            success: false,
            status: 403
        } as IResponse);

    } catch (e) {
        if (e.kind === 'ObjectId' && e.path === '_id') {
            return res.status(403).json({
                message: `Ooops, something broken !`,
                success: false,
                status: 403
            } as IResponse);
        }
        next(e);
    }
};