import {NextFunction, Request, Response} from 'express';
import {UserModel} from "../../common/models/UserModel";
import {IResponse} from "../../common/interfaces/IResponse";
import {UserClass} from "../../common/classes/UserClass";
import {signToken} from "../../common/helpers/jsonWebToken";

export async function login(req: Request, res: Response, next: NextFunction) {
    try {
        const value = req.body.value;
        const user: UserClass = await UserModel.findOne({
            $or: [
                {'local.username': value.login},
                {'email.address': value.login},
                {'phone.number': value.login},
            ]
        });
        if (!user) {
            return res.status(404).json({
                message: 'Account not found, please create account',
                success: false,
                status: 404
            } as IResponse);
        }
        if (!user.isValid(value.password)) {
            return res.status(403).json({
                message: 'Password incorrect',
                success: false,
                status: 403
            } as IResponse);
        }
        if (!user.email.isVerified && !user.phone.isVerified) {
            return res.status(203).json({
                message: 'Please verify your account',
                success: false,
                status: 203,
                data: {
                    uuid: user.id
                }
            } as IResponse);
        }
        return res.status(200).json({
            message: 'OK',
            data: signToken(user),
            success: true,
            status: 200
        } as IResponse);
    } catch (e) {
        next(e);
    }
}