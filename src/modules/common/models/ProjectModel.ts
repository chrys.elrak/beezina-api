import { model,Model,Schema  } from 'mongoose';
import { ProjectClass } from '../classes/ProjectClass';

const ProjectSchema: Schema<ProjectClass> = new Schema({

    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    investor: {
        type: Array,
        default: [],
        required: false
    },
    // category: {
    //     type: String,
    //     required: true,
    // },
    owner: {
        type: Schema.Types.ObjectId,
        ref:"User",
        required: true
    },
    votes: {
        type: Object,
        default: {
            public: [],
            sponsor: []
        },
        required: false
    },
    comments: {
        type: Array,
        default: [],
        required: false
    },
    attachment: {
        type: String,
        required: false
    },
    info: {
        type: String,
        required: false
    }

});

export const ProjectModel: Model<ProjectClass> = model('Project', ProjectSchema);