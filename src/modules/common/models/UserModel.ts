import bcrypt from "bcrypt";
import {model, Model, Schema} from 'mongoose';
import {UserClass} from "../classes/UserClass";
import {eMethods} from "../enums/eMethods";

const UserSchema: Schema<UserClass> = new Schema({
    email: {
        address: {
            type: String,
            required: true,
        },
        isVerified: {
            type: Boolean,
            default: false,
        },
        code: {
            type: String,
            default: '',
        }
    },
    phone: {
        number: {
            type: String,
            required: false
        },
        isVerified: {
            type: Boolean,
            default: false,
        },
        code: {
            type: String,
            default: '',
        }
    },
    local: {
        username: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true,
        }
    },
    google: {
        id: {
            type: String,
            // required: true
        },
        name: {
            type: String,
            // required: true
        },
        given_name: {
            type: String,
            // required: true
        },
        family_name: {
            type: String,
            // required: true
        },
        email: {
            type: String,
            // required: true
        },
        picture: {
            type: String,
            // required: true
        },
    },
    method: {
        enum: ['local', 'google'],
        required: true,
        default: 'local',
        type: String
    },
    gender: {
        enum: ['male', 'female'],
        required: true,
        default: 'male',
        type: String
    },
    role: {
        enum: ['sponsor', 'project_owner'],
        required: true,
        default: 'project-owner',
        type: String
    },
    info: {
        type: Object,
        default: {}, required: false
    }
});

UserSchema.methods.isValid = function (password: string): boolean {
    return bcrypt.compareSync(password, this.local.password);
};

// UserSchema.pre<UserClass>('save', function (next) {
//     if (this.method.toString() !== eMethods[eMethods.local]) return next(null);
//     const salt = bcrypt.genSaltSync(10);
//     this.local.password = bcrypt.hashSync(this.local.password, salt);
//     next(null);
// });

export const UserModel: Model<UserClass> = model('User', UserSchema);

