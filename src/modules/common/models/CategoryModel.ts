import { CategoryClass } from "common/classes/CategoryClass";
import { model,Model,Schema} from "mongoose";

const SchemaCategory: Schema<CategoryClass> = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        require: true
    }
});


export const CategoryModel: Model<CategoryClass> = model('Category', SchemaCategory);
