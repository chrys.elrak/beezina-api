import * as Joi from 'joi';

const CategorySchema = Joi.object({
    name: Joi.string().required(),
    description: Joi.string().min(5).required()
})

export { CategorySchema }