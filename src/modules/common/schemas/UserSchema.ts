import * as Joi from "joi";

export const registerObject = {
    username: Joi.string().alphanum().min(2).required(),
    password: Joi.string().min(6).required(),
    role: Joi.string().regex(/sponsor|project_owner/).required(),
    phone: Joi.string().optional(), // TODO: Regex
    gender: Joi.string()
        .regex(/male|female/)
        .required(),
    email: Joi.string().email().required(),
};

export const loginObject = {
    login: Joi.string().required(),
    password: Joi.string().min(6).required(),
};

export const code_validation = Joi.object({
    code: Joi.string().alphanum().uppercase().length(6).required(),
    // email: Joi.string().alphanum().uppercase().length(6).required(),
    // phone: Joi.string().alphanum().uppercase().length(6).optional(),
});

const register = Joi.object(registerObject);
const login = Joi.object(loginObject);
const reset_password = Joi.object({
    id: Joi.string().required(),
    password: Joi.string().min(6).required(),
    // repeat_password: Joi.ref("password")
});

export default {
    register,
    login,
    reset_password
}