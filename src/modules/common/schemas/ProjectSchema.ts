import * as Joi from 'joi';

export const registerProjectObject = {
    title: Joi.string().required(),
    description: Joi.string().required(),
    category: Joi.string().required(),
    attachment: Joi.optional(),
    info: Joi.string().required(),
};


export const projectSchema = Joi.object(registerProjectObject);

