import { config } from '../.././../config';
import Mongoose from 'mongoose';

export function connection() {
    Mongoose.connect(config.dbhost, {
        dbName: config.dbname,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    })
        .then(() => {
            console.log('Mongo : Conected ✔️');
        })
        .catch((err) => {
            console.log('Mongo : Not connected ️❌️');
            throw err;
        });
}
