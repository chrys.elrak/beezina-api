import sgMail = require('@sendgrid/mail')
import { IUser } from "common/interfaces/IUser";
import {config} from '../../../config';


sgMail.setApiKey(config.sendgrid_api_key);

export const sendEmailValidation = async (user: IUser): Promise<any> => {
    const msg = {
        to: user.email.address, // Change to your recipient
        from: config.sendgrid_sender, // Change to your verified sender
        subject: 'Validation email | BeeZina',
        html: `
            <html> 
                <head>
                    <style>
                         @import url('https://fonts.googleapis.com/css2?family=Lora:ital@1&display=swap'); 
                        body {
                            font-family: 'Lora', serif;
                            color: whitesmoke;
                        }
                        .container {
                            width: fit-content;
                            margin: auto;
                            padding: 20px;
                            text-align: center;
                            background-color: cornflowerblue;
                        }
                        .yellow {
                            color: #E09722;
                        }
                        .black {
                            color: #31240E;
                        }
                        .code {
                            display: inline-block;
                            margin: 8px 0;
                            padding: 8px 25px;
                            font-size: 16px;
                            border: 1px dashed whitesmoke;
                        }
                    </style>
                </head>    
                <body>
                    <div class="container">
                        <h1>Welcome to <span class="yellow">Bee</span><span class="black">Zina</span></h1>
                        <p>
                            Copy and paste the code validation into the page confirmation.<br/>
                            Code: <span class="code yellow">${user.email.code}</span>
                        </p>
                    </div>
                </body>        
            </html>
            `,
    };
    await sgMail
        .send(msg)
        .then(() => {
            console.log('Email sent')
        })
        .catch((error) => {
            console.error(error)
        });
};
