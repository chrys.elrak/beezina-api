import { IUser } from "common/interfaces/IUser";
import Twilio from 'twilio';
import { config } from "../../../config";


const accountSid = config.twilioAccountSID;
const authToken = config.twilioAuthToken;
const twilioPhoneNumber = config.twilioPhoneNumber;
const client = Twilio(accountSid, authToken);

export const sendPhoneNumberValidation = async (user: IUser) => {
    let contentText = {
        body: `There is your confirmation code for your phone number to beezina:
        ${user.phone.code}. Thanks!`,
        from: twilioPhoneNumber,
        to: user.phone.number

    };
    await client.messages
        .create(contentText)
        .then(message => console.log(message.body, + " => ", + message.status))
        .catch(e => console.log("SMS error: " + e.message));
};