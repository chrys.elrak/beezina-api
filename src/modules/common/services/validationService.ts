import {IUser} from "common/interfaces/IUser";
import {sendEmailValidation} from "./sendEmailValidation";
import {sendPhoneNumberValidation} from "./sendSMSValidation";

export const sendCodeValidation = async (user: IUser) => {
    if (user.email.code && !user.email.isVerified) {
        await sendEmailValidation(user);
    }
    if (user.phone.code && !user.phone.isVerified) {
        await sendPhoneNumberValidation(user);
    }
};