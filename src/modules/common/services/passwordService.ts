import bcrypt from "bcrypt";

export const cryptoPassword = (pass: string): string => {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(pass, salt);
}