import { ProjectClass } from "common/classes/ProjectClass";
import { UserClass } from "common/classes/UserClass";
import { generateCode } from "common/helpers/string";
import { IResponse } from "common/interfaces/IResponse";
import { ProjectModel } from "common/models/ProjectModel";

export async function getProjects(): Promise<any[]> {
    return  ProjectModel.find({})
    .populate('owner', ['email', 'gender', 'local', 'phone'])
    .populate('category')
    .populate('comments.author', ['local.username', 'comments.createdAt'], 'User')
    .populate('investor', ['email', 'gender', 'phone', 'local.username',  '_id'], 'User');
}

export async function getBy(params: any) {
    console.log('GET MINE')
    return ProjectModel
            .find(params)
            .populate('category')
            .populate('comments.author', ['local.username', 'comments.createdAt'], 'User')
            .populate('investor', ['email', 'gender', 'phone', 'local.username', '_id'], 'User');
}

export async function getPublicProjects() {
    return ProjectModel
    .find({}, {info: 0, comments: 0, investor: 0, 'votes.sponsor': 0})
    .populate('category');
}

export async function investProject(params: {userId: any, projectId: any}): Promise<IResponse> {
    const project: ProjectClass = await ProjectModel.findById(params.projectId);
        if (!project) {
            return ({
                status: 404,
                message: "Project not found",
                success: false
            } as IResponse);
        }
        const exist = project.investor
            .find(id => id === params.userId);
        if (!exist) {
            // INVEST
            project.investor.push(params.userId);
        } else {
            // REMOVE INVEST
            project.investor = project.investor
                .filter(id => id !== params.userId);
        }
        await project.update(project);
        return ({
            status: 200,
            message: `Project id '${params.projectId}' was invested successfully`,
            success: true,
            data: project
        } as IResponse);
}

export async function voteProject(params: {user: UserClass, projectId: any, phoneNumber?: any}): Promise<IResponse> {
    console.log('VOTE')
    const project: ProjectClass = await ProjectModel.findById(params.projectId);
    if (!project) {
        return ({
            message: "Project not found",
            success: false,
            status: 404,
        } as IResponse);
    }
    // VOTE HERE
    // CHECK VERIFICATION

    if (params.user.role === 'public' && params.phoneNumber) {
        // SEND VOTE
        project.votes.public.push({
            code: generateCode(6),
            createdAt: new Date(),
            phoneNumber: params.phoneNumber || 'No number'// TODO: SECURE PHONE NUMBER => VALIDATION NEEDED
        });
    } else if (params.user.role === 'sponsor') {
        const exist = project.votes.sponsor
            .find(e => e.user === params.user._id);
        if (!exist) {
            // SEND VOTE
        console.log('SEND VOTE', params.user)
            project.votes.sponsor.push({
                user: params.user._id,
                createdAt: new Date(),
            });
        } else {
            // REMOVE VOTE
            project.votes.sponsor = project.votes.sponsor
                .filter(e => e.user !== params.user._id);
        }
    }
    await project.update(project);

    return({
        message: `Project id '${params.projectId}' voted successfully`,
        success: true,
        status: 200,
    } as IResponse);
}

export async function sortProject(idUser: any) {
    const response: {projects: any[], sponsorisedProjects?: any[]} = {
        projects: await getProjects(),
      };
      response.sponsorisedProjects = response.projects.filter((p: any) =>
        p.investor.find((pp: any) => pp._id.toString() === idUser)
      );
      return response;
}