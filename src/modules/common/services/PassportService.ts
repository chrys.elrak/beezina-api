import passport from 'passport';
import {Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt';
import {config} from '../../../config';
import {UserClass} from "../classes/UserClass";
import {UserModel} from "../models/UserModel";
import {Strategy as GoogleStrategy} from "passport-google-oauth20";

export async function JwtAuth() {
    passport.use('jwt',
        new JwtStrategy({
                jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
                secretOrKey: config.secretToken,
            },
            async (payload, done) => {
                try {
                    const user: UserClass = await UserModel.findById(payload.sub.uuid, {"local.password": 0});
                    if (!user) {
                        return done(null, false);
                    }
                    done(null, user);
                } catch (e) {
                    done(e, false);
                }
            }
        )
    );
}

export function initGoogleOAuth() {
    passport.use(new GoogleStrategy({
            clientID: config.googleClientId as string,
            clientSecret: config.googleClientSecret as string,
            callbackURL: config.googleCallbackUrl as string
        },
        (accessToken: string, refreshToken: string, profile: any, done: Function) => {
            console.log(profile);
            return done(null, profile);
        }
    ));
}

