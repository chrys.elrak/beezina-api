import {ObjectId} from "mongoose";

export interface IComment {
    createdAt: Date;
    author: ObjectId | string;
    comment: string;

}
