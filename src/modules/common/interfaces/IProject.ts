import {IVote} from "./IVote";
import {IComment} from "./IComment";

export interface IProject {
    title: string;
    description: string;
    category: string;
    owner: string;
    votes: IVote;
    investor: string[];
    comments: IComment[],
    info?: any;
    attachment?: any[]
    createdAt: Date;
}