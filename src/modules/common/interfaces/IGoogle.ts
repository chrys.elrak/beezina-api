export interface IGoogle {
    id: string;
    name: string;
    given_name: string;
    family_name: string;
    picture: string;
    email: string;
    email_verified: string;
    locale: string;
}
