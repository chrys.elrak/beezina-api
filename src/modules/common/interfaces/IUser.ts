import {IEmail} from "./IEmail";
import {IPhone} from "./IPhone";
import {ILocal} from "./ILocal";
import {IGoogle} from "./IGoogle";
import {eMethods} from "../enums/eMethods";
import {eGender} from "../enums/eGender";

export interface IUser {
    email: IEmail;
    phone: IPhone;
    method: eMethods;
    local: ILocal;
    google: IGoogle;
    gender: eGender;
    role: string;
}
