export interface IEmail {
    isVerified: boolean;
    address: string;
    code: string;
}