export interface IPhone {
    isVerified: boolean;
    number: string;
    code: string;
}