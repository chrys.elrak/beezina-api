export interface IVote {
    sponsor: Array<{ readonly user: string, readonly createdAt: Date }>;
    public: Array<{readonly phoneNumber: string, readonly createdAt: Date, readonly code: string}>
}