export const validateEmail = function(email: string) {
    const re = /^\w+([z\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};