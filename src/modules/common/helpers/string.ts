/**
 * Generate code
 * @param length: code length (by default: 6)
 * @param mode: type of code to generate (by default: 'alphanumeric') {'characters', 'numeric'}
 */
export const generateCode = (length: number = 6, mode: string = "alphanumeric"): string => {
    let code: string = '';
    let characters: string;
    let cLength: number;

    switch (mode) {
        case "alphanumeric":
            characters = 'qwertyuiopasdfghjklzxcvbnm1234567890'.toUpperCase();
            break;
        case "characters":
            characters = 'qwertyuiopasdfghjklzxcvbnm'.toUpperCase();
            break;
        case "numeric":
            characters = '1234567890';
            break;
        default:
            throw (new Error(`${mode} is invalid parameter of generateCode. There are the valid value of 'mode': 'alphanumeric', 'characters', 'numeric'`));
    }
    cLength = characters.length;
    for (let i=0; i<length; i++) {
        code += characters.charAt(Math.floor(Math.random() * cLength));
    }
    return code;
};