import jwt from 'jsonwebtoken';
import { config } from '../../../config';
import {UserClass} from "../classes/UserClass";
import moment from 'moment';

export function signToken(user: UserClass): string {
    const token = {
        iss: config.issuerToken,
        sub: {
            uuid: user._id,
            role: user.role
        },
        iat: moment().toDate().getTime(),
    };
    console.log(token);
    return jwt.sign(token, config.secretToken, {expiresIn: '1h'})
}


export function decodeToken(token: string): string | { [key: string]: string; } | null {
    return jwt.decode(token);
}
