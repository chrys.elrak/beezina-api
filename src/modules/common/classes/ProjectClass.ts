import {Document} from "mongoose";
import {IProject} from "../interfaces/IProject";
import {IVote} from "../interfaces/IVote";
import {IComment} from "../interfaces/IComment";

export abstract class ProjectClass extends Document implements IProject {
    protected constructor(
        public title: string,
        public description: string,
        public category: string,
        public owner: string,
        public votes: IVote,
        public comments: IComment[],
        public createdAt: Date,
        public investor: string[],
        public attachment?: any[],
        public info?: any[]
    ) {
        super();
    }
}
