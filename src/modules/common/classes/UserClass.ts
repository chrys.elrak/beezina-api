import {Document} from "mongoose";
import {IUser} from "../interfaces/IUser";
import {IEmail} from "../interfaces/IEmail";
import {IPhone} from "../interfaces/IPhone";
import {ILocal} from "../interfaces/ILocal";
import {IGoogle} from "../interfaces/IGoogle";
import {eMethods} from "../enums/eMethods";
import {eGender} from "../enums/eGender";

export abstract class UserClass extends Document implements IUser {
    protected constructor(
        public email: IEmail,
        public phone: IPhone,
        public method: eMethods,
        public createdAt: Date,
        public local: ILocal,
        public google: IGoogle,
        public gender: eGender,
        public role: string
    ) {
        super();
    }

    abstract isValid(password: string): Boolean;
}