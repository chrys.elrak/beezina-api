import { ICategory } from 'common/interfaces/ICategory';
import { Document } from 'mongoose';


export class CategoryClass extends Document implements ICategory{

    constructor(
        public name: string,
        public description: string
    ){
        super()
    }

}