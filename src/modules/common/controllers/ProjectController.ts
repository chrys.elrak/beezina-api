import {IResponse} from 'common/interfaces/IResponse';
import {NextFunction, Request, Response} from 'express'
import {ProjectModel} from '../models/ProjectModel'
import {generateCode} from "../helpers/string";
import {UserClass} from "../classes/UserClass";
import {registerProjectObject} from "../schemas/ProjectSchema";
import {IProject} from "../interfaces/IProject";
import {ProjectClass} from "../classes/ProjectClass";
import {IComment} from "../interfaces/IComment";
import { getBy, getProjects, getPublicProjects, investProject } from 'common/services/ProjectService';


// create one project
export async function registerProject(req: Request, res: Response, next: NextFunction) {
    try {
        const userId = (req.user as UserClass)?.id;
        const value: typeof registerProjectObject = req.body.value;
        const project = new ProjectModel({
            title: value.title,
            description: value.description,
            attachment: value.attachment,
            category: value.category,
            info: value.info,
            owner: userId
        });
        await project.save();
        return res.status(201).json({
            message: 'Project created...',
            success: true,
            status: 201
        } as IResponse);
    } catch (e) {
        next(e);
    }
}

export async function removeProject(req: Request, res: Response, next: NextFunction) {
    try {
        const project: ProjectClass = await ProjectModel.findById(req.params.projectId);
        if (!project || JSON.stringify(project.owner) !== JSON.stringify((req.user as UserClass).id)) {
            return res.status(404)
                .json({
                    status: 404,
                    success: false,
                    message: "Delete failed"
                } as IResponse);
        }
        await project.delete();
        return res.status(200)
            .json({
                status: 200,
                success: true,
                message: "Project deleted successfully..."
            } as IResponse);
    } catch (e) {
        next(e);
    }
}

export async function updateProject(req: Request, res: Response, next: NextFunction) {
    try {
        const projectId: string = req.params.projectId;
        let doc: ProjectClass = await ProjectModel.findById(projectId);
        if (!doc || JSON.stringify(doc?.owner) !== JSON.stringify((req.user as UserClass)?._id)) {
            return res.status(404).json({
                status: 404,
                message: `Project ${projectId} not found`,
                success: false
            } as IResponse);
        }
        await doc.update(req.body);
        return res.status(200).json({
            status: 200,
            success: true,
            message: `Project id: ${req.params.projectId} updated`,
        } as IResponse);
    } catch (e) {
        next(e);
    }
}

export async function getAllProjectsWithoutDetail(req: Request, res: Response, next: NextFunction) {
    try {
        const projectList = await getPublicProjects();
        return res.status(200).json({
            message: "list of project",
            success: true,
            status: 200,
            data: projectList
        } as IResponse);
    } catch (e) {
        next(e);
    }
}

export async function getAllOwnProjects(req: Request, res: Response, next: NextFunction) {
    try {
        const userId = (req.user as UserClass)?.id;
        const allProjects = await getBy({"owner": userId});
        return res.status(200)
            .json({
                message: `All ny projects`,
                success: true,
                status: 200,
                data: allProjects
            } as IResponse);
    } catch (e) {
        next(e);
    }
}

/**
 * getAllProjects: master list with detail
 */
export async function getAllProjects(req: Request, res: Response, next: NextFunction) {
    try {
        const data = await getProjects();
        return res.status(200).json({
            message: 'Master list of project',
            success: true,
            status: 200,
            data: data
        } as IResponse);
    } catch (e) {
        next(e);
    }
}

/**
 * getOneProject: one project
 * params: projectId
 */
export async function getOneProject(req: Request, res: Response) {
    try {
        const projectId: string = req.params.projectId;
        const project: IProject = await ProjectModel.findById(projectId);
        if (!project) {
            return res.status(404).json({
                status: 404,
                message: `Project ${projectId} not found`,
                success: false
            } as IResponse);
        }
        return res.status(200).json({
            status: 200,
            message: `Project id '${projectId}' fetch successfully`,
            success: true,
            data: project
        } as IResponse);
    } catch (e) {
        return res.status(500).json({
            status: 500,
            message: e,
            success: false
        } as IResponse);
    }
}

/**
 * Vote project
 * @param who is the person to vote
 */
export function voteProject(who: string) {
    return async (req: Request, res: Response) => {
        try {
            const projectId: string = req.params.projectId;
            const project: ProjectClass = await ProjectModel.findById(projectId);
            if (!project) {
                return res.status(404).json({
                    message: "Project not found",
                    success: false,
                    status: 404,
                } as IResponse);
            }
            // VOTE HERE
            // CHECK VERIFICATION
            if (!who || who !== 'public' && who !== 'sponsor') {
                console.error(`
                ERROR API: method voteProject into the ProjectController need a string parameter:
                    the parameter only possible are: public or sponsor.
                This error was provided by the call function voteProject in controller without/bad parameter
                `);
                return res.status(500).json({
                    status: 500,
                    success: false,
                    message: "Something broke!"
                } as IResponse);
            }
            if (who === 'public') {
                // SEND VOTE
                project.votes.public.push({
                    code: generateCode(6),
                    createdAt: new Date(),
                    phoneNumber: req.body.phoneNumber || 'No number'// TODO SECURE PHONE NUMBER => VALIDATION NEEDED
                });
            } else if (who === 'sponsor') {
                const exist = project.votes.sponsor
                    .find(e => e.user === (req.user as UserClass).id);
                if (!exist) {
                    // SEND VOTE
                    project.votes.sponsor.push({
                        user: (req.user as UserClass).id,
                        createdAt: new Date(),
                    });
                } else {
                    // REMOVE VOTE
                    project.votes.sponsor = project.votes.sponsor
                        .filter(e => e.user !== (req.user as UserClass).id);
                }

            }

            await project.update(project);

            return res.status(200).json({
                message: `Project id '${projectId}' voted successfully`,
                success: true,
                status: 200,
            } as IResponse);
        } catch (e) {
            console.error("VOTE PROJECT ERROR: ", e);
            return res.status(500).json({
                status: 500,
                message: e,
                success: false
            } as IResponse);
        }
    }
}

/**
 * Invest one project
 * @param req
 * @param res
 */
export async function invest(req: Request, res: Response) {
    try {
        const projectId: string = req.params.projectId;
        const response: Promise<IResponse> =  investProject({ userId: (req.user as UserClass).id, projectId})
        return res.status((await response).status).json(await response);
    } catch (e) {
        return res.status(500).json({
            status: 500,
            message: e,
            success: false
        } as IResponse);
    }
}


/**
 * Comment one project
 * @param req
 * @param res
 */
export async function comment(req: Request, res: Response, next: NextFunction) {
    try {
        const project = await ProjectModel.findById(req.params.projectId);
        if (!project) {
            return res.status(404).json({
                message: "Project not found",
                success: false,
                status: 404,
            } as IResponse);
        }
        const objComment: IComment = {
            comment: req.body.comment || req.body.value,
            createdAt: new Date(),
            author: (req.user as UserClass).id
        } // TODO schema objComment
        // CHECK VERIFICATION
        // comment
        project.comments.push(objComment);

        await project.save();

        return res.status(200).json({
            message: "Project commented...",
            success: true,
            status: 200,
        } as IResponse);
    }
    catch (e) {
        next(e);
    }

}
