import {NextFunction, Request, Response} from "express";
import {eRoles} from "../enums/eRoles";
import {UserClass} from "../classes/UserClass";
import {IResponse} from "../interfaces/IResponse";

export default function (role: eRoles) {
    return (req: Request, res: Response, next: NextFunction) => {
        if ((req.user as UserClass)?.role !== eRoles[role]) {
            return res.status(401).json({
                status: 401,
                success: false,
                message: 'Unauthorized role'
            } as IResponse);
        }
        next();
    }
}