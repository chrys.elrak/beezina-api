import {Router} from "express";
import passport from "passport";
import is from "common/middlewares/roleChecker";
import {eRoles} from "common/enums/eRoles";
import {comment, getAllProjects, getOneProject, invest, voteProject} from "../common/controllers/ProjectController";

/**
 * SPONSOR
 */
export const router = Router();

router.route('/projects')
    .get(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.sponsor),
        getAllProjects
    );

router.route('/project/:projectId')
    .get(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.sponsor),
        getOneProject
    );

router.route('/project/vote/:projectId')
    .patch(passport.authenticate('jwt', {session: false}),
        is(eRoles.sponsor),
        voteProject('sponsor')
    );

router.route('/project/invest/:projectId')
    .patch(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.sponsor),
        invest
    );

router.route('/project/comment/:projectId')
    .patch(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.sponsor),
        comment
    );