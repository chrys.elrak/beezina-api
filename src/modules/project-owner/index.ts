import {Router} from "express";
import passport from "passport";
import is from "../common/middlewares/roleChecker";
import {eRoles} from "../common/enums/eRoles";
import {validate} from 'common/middlewares/formValidation'

import {projectSchema} from "common/schemas/ProjectSchema";
import {
    getAllOwnProjects,
    getAllProjectsWithoutDetail,
    getOneProject,
    registerProject, removeProject, updateProject
} from "../common/controllers/ProjectController";

/**
 * PROJECT OWNER
 */
export const router = Router();

/**
 * Get all projects
 */
router.route('/projects/all')
    .get(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.project_owner),
        getAllOwnProjects
    );

/**
 * Get all projects no detail
 */
router.route('/projects/no-detail')
    .get(getAllProjectsWithoutDetail);

/**
 * Get all own projects
 */
router.route('/projects/own')
    .get(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.project_owner),
        getAllOwnProjects
    );

/**
 * Create project
 */
router.route('/project')
    .post(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.project_owner),
        validate(projectSchema),
        registerProject
    );


/**
 * Read, Update and Delete project
 */
router.route('/project/:projectId')
    .get(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.project_owner),
        getOneProject
    )
    .put(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.project_owner),
        updateProject
    )
    .delete(
        passport.authenticate('jwt', {session: false}),
        is(eRoles.project_owner),
        removeProject
    );