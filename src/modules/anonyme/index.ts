import {Router} from "express";
import {getAllProjectsWithoutDetail, voteProject} from '../common/controllers/ProjectController'

/**
 * ANONYME
 */
export const router = Router();

router.route('/public/projects')
    .get(getAllProjectsWithoutDetail);

router.route('/public/project/vote/:projectId')
    .patch(voteProject('public'));
