import * as dotenv from 'dotenv';
import assert from 'assert';

dotenv.config();

assert(process.env.PORT, "PORT is required");
assert(process.env.DB_HOST, "DB_HOST is required");
assert(process.env.DB_NAME, "DB_NAME is required");
assert(process.env.SECRET_TOKEN, "SECRET_TOKEN is required");
assert(process.env.SHARED_FOLDER, "SHARED_FOLDER is required");
assert(process.env.SENDGRID_API_KEY, "SENDGRID_API_KEY is required");
assert(process.env.SENDGRID_SENDER, "SENDGRID_SENDER is required");
assert(process.env.TWILIO_ACCOUNT_SID, "TWILIO_ACCOUNT_SID is required");
assert(process.env.TWILIO_AUTH_TOKEN, "TWILIO_AUTH_TOKEN is required");
assert(process.env.TWILIO_PHONE_NUMBER, "TWILIO_PHONE_NUMBER is required");
assert(process.env.GOOGLE_CLIENT_ID, "GOOGLE_CLIENT_ID is required");
assert(process.env.GOOGLE_CLIENT_SECRET, "GOOGLE_CLIENT_SECRET is required");
assert(process.env.GOOGLE_CLIENT_SECRET, "GOOGLE_CALLBACK_URL is required");

export const config = {
    host: process.env.HOST,
    hostUrl: process.env.HOST_URL,
    port: process.env.PORT,
    dbname: process.env.DB_NAME,
    dbhost: process.env.DB_HOST,
    secretToken: process.env.SECRET_TOKEN,
    issuerToken: process.env.ISS_TOKEN,
    baseUrl: process.env.BASE_URL,
    sharedFilePath: process.env.SHARED_FOLDER,
    sendgrid_api_key: process.env.SENDGRID_API_KEY,
    sendgrid_sender: process.env.SENDGRID_SENDER,
    twilioAccountSID: process.env.TWILIO_ACCOUNT_SID,
    twilioAuthToken: process.env.TWILIO_AUTH_TOKEN,
    twilioPhoneNumber: process.env.TWILIO_PHONE_NUMBER,
    NODE_ENV: process.env.NODE_ENV,
    googleClientId: process.env.GOOGLE_CLIENT_ID,
    googleClientSecret: process.env.GOOGLE_CLIENT_SECRET,
    googleCallbackUrl: process.env.GOOGLE_CALLBACK_URL
};
