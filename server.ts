import cors from "cors";
import express, { NextFunction, Request, Response } from "express";
import { config } from "./src/config";
import { connection } from "./src/modules/common/services/DBService";
import { router } from "./src/router";
import helmet from "helmet";
import { IResponse } from "./src/modules/common/interfaces/IResponse";
import {
  getBy,
  getProjects,
  investProject,
  sortProject,
  voteProject,
} from "common/services/ProjectService";
import { use } from "passport";
import { UserClass } from "common/classes/UserClass";

connection();
const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);

app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use("/api/v1", router);

app.route("/").get((req: Request, res: Response) => res.sendStatus(200));

// @ts-ignore
app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
  console.log(err);
  if (err?.kind === "ObjectId") {
    return res.status(404).json({
      status: 404,
      message: "ID not found !",
      success: false,
    } as IResponse);
  }
  return res.status(500).json({
    status: 500,
    success: false,
    message:
      config.NODE_ENV === "dev"
        ? err.message
        : "Oopss, something went wrong :(",
    data: err,
  } as IResponse);
});

app.all("/**", (req: Request, res: Response) =>
  res.status(404).send("Page not found")
);

io.on("connection", async (socket: any) => {
  socket.on("sponsor", async (idUser: any) => {
    socket.emit("projects", await sortProject(idUser));
  });

  socket.on("project-owner", async (idUser: any) => {
    const response = await getBy({ owner: idUser });
    socket.emit("projects", response);
  });

  socket.on("invest", async (params: { user: any; project: any }) => {
    const response = await investProject({
      projectId: params.project._id,
      userId: params.user._id,
    });

    if (response.success) {
      return socket.emit("projects", await sortProject(params.user._id));
    }
  });

  socket.on(
    "sponsor:vote",
    async (params: { user: UserClass; project: any }) => {
      const response = await voteProject({
        projectId: params.project._id,
        user: params.user,
      });

      if (response.success) {
        return socket.emit("projects", await sortProject(params.user.id));
      }
    }
  );
});

http.listen(config.port, () => {
  console.log("Server running on port " + config.port);
});
