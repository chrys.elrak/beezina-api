# Beezina

Beezina is a Node.js app for Investisor and project

## Installation

Use the package manager [npm](https://npmjs.com) to install dependencies.

```bash
npm install
```

## Run

Make sure that you have .env file with needed environements variables

This command will create dist folder

```bash
npm run startAll
```
but

```bash
npm run start
```

or 

```bash
npm start
```

will not create dist folder
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)